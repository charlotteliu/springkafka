package com.bluedream.kafka.aptest1.producer.controller;

import com.bluedream.kafka.aptest1.producer.KafkaMessageSendService1;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value="send",produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
public class KafkaMessageSend1Controller {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageSendService1.class);

    @Autowired
    private KafkaMessageSendService1 kafkaMessageSendService;

    @ApiOperation(value="發送訊息", notes="使用Kafka 發送訊息")
    @ApiImplicitParam(name = "message", value = "訊息內容", required = true, dataType = "String")
    @RequestMapping(value="/sendMessage",method=RequestMethod.POST)
    public String send(@RequestParam(required=true) String message){
        try {
            kafkaMessageSendService.sendByMesg(message);
        } catch (Exception e) {
            return "send failed.";
        }
        return message;
    }

}
