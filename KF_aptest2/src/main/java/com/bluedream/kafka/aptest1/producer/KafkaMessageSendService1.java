package com.bluedream.kafka.aptest1.producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Service
public class KafkaMessageSendService1 {
    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageSendService1.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.app.topic.topic001}")
    private String topic;


    public void sendByMesg(String pPlayload) {
        LOG.info("topic="+topic+",message="+pPlayload);

        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, pPlayload);
        future.addCallback(success -> LOG.info("KafkaMessageProducer send message is successful！"),
                fail -> LOG.error("KafkaMessageProducer is failed！"));


    }
}
