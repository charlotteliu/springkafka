package com.bluedream.kafka.aptest1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Charlotte Liu
 * @version 1.0.0
 * @date 06/30/2019 下午12:02.
 * @blog https://github.com/Bluedream007
 */

@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bluedream.kafka.aptest1.producer.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Use SpringBoot2-Swagger2 to build the RESTful APIs for Kafka demo test1")
                .description("More information : https://github.com/Bluedream007")
                .termsOfServiceUrl("https://github.com/Bluedream007")
                .contact("Charlotte")
                .version("1.0")
                .build();
    }
}
