package com.bluedream.kafka.aptest1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KFaptest1Application {

	public static void main(String[] args) {
		SpringApplication.run(KFaptest1Application.class, args);
	}

}
