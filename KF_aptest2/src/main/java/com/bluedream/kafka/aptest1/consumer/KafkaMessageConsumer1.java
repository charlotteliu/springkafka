package com.bluedream.kafka.aptest1.consumer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class KafkaMessageConsumer1 {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaMessageConsumer1.class);

    // just for DEV test
    private CountDownLatch latch = new CountDownLatch(1);

    public CountDownLatch getLatch() {
        return latch;
    }

    @Value("${kafka.app.topic.topic001}")
    private String topic;

    @KafkaListener(topics={"${kafka.app.topic.topic001}"})
    public void receive(String payload) {
        LOG.info("received payload='{}'", payload);
        latch.countDown();
    }
}
