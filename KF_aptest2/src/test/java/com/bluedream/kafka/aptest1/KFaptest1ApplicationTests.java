package com.bluedream.kafka.aptest1;

import com.bluedream.kafka.aptest1.consumer.KafkaMessageConsumer1;
import com.bluedream.kafka.aptest1.producer.KafkaMessageSendService1;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@EmbeddedKafka(partitions = 1,
		topics = {KFaptest1ApplicationTests.TEST_TOPIC1})
public class KFaptest1ApplicationTests {

	@Test
	public void contextLoads() {
	}

	static final String TEST_TOPIC1 = "topic001";

	@Autowired
	private KafkaMessageConsumer1 receiver;

	@Autowired
	private KafkaMessageSendService1 sender;

	@Test
	public void testReceive() throws Exception {
		sender.sendByMesg("Hello Spring Kafka!");

		receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
		assertThat(receiver.getLatch().getCount()).isEqualTo(0);
	}

}
