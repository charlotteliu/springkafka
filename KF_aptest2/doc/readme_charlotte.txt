Ref. Link:
https://www.cnblogs.com/guanzhyan/p/8979050.html
https://codenotfound.com/spring-kafka-consumer-producer-example.html
- with group, filter
  https://www.baeldung.com/spring-kafka
- swagger2
  http://blog.didispace.com/springbootswagger2/

* ----------------------------------- *
* Unit Test
- Maven
  > mvn test


* ----------------------------------- *
* Run
* Kafka CLI
- consumer
docker exec kafkadocker_kafka_1 \
kafka-console-consumer.sh \
--topic topic001 \
--bootstrap-server kafkadocker_kafka_1:9092,kafkadocker_kafka_2:9092
- maven
 > mvn spring-boot:run




* ----------------------------------- *
* Test Data
- send by Postman
  http://localhost:8000/send/sendMessage/?message=messageTest2

- Send by Swagger2 UI
  http://localhost:8000/swagger-ui.html

test



